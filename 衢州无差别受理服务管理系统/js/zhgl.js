// JavaScript Document
$(function(){
	//点击地区选择
	$(".place-table .span-hover").click(function(){
		$(this).siblings().attr('class','span-hover');
		$(this).attr('class','span-active');
	});
	
	$('.color-organ').click(function(){
		$(".place-table").slideDown();
	});
	$(".sure").click(function(){
		$(".place-table").slideUp();
	});
	//设置无法点击事件
	 $(".disabled").click(function (event) {
		   event.preventDefault();        
	  });
	//点击行业选择
	$(".div_profess .li-hover").click(function(){		
		$(this).attr('class','li-active');
		var id=$(this).data('id');
		document.getElementById("icon_img"+id).src="images/index-icon-"+id+"-2.png";    //更改li中的图片链接
		
		var value=$(this).children('h4').text();
		//获取文本宽度
		var sensor = $('<pre>'+ value+'</pre>').css({display: 'none'}); 
        $('body').append(sensor); 
        var width = sensor.width()+8;
		
		var html='<div><div class="confire_things">';
		html+='	<input type="buttom" value="'+value+'" style="width:'+width+'px" data-id="'+id+'">';
		html+='	</div>';
		html+='	<div class="thing_div div_thing">';
		html+='	<label><input type="checkbox" name="reason" checked><i>✓</i>特种行业许可证</label> ';
		html+='	<label><input type="checkbox" name="reason" checked><i>✓</i>公共场所卫生许可证</label> ';
		html+='	<label><input type="checkbox" name="reason" checked><i>✓</i>公众聚集场所投入使用、营业前消防安全检查合格证</label> ';		
		html+='	</div></div>';	
		$('.confire_box').append(html);
		$(this).addClass('disabled');  
	});
	//点击选中的标题删除改行业的选择
	$('.confire_box').on('click','.confire_things input',function(){
		$(this).parent().parent().hide();
		$(this).parent().siblings('.thing_div').children().children('input').attr("checked",false);//全不选 
		var id=$(this).data('id');
		document.getElementById("icon_img"+id).src="images/index-icon-"+id+".png";    //更改li中的图片链接
		$('#icon-li'+id).attr('class','li-hover');
		$('#icon-li'+id).attr("disabled",false);
	});
	//自动点击出现弹框，选择工商图标
	$('#choice-buttom').click();
	$(".jshref").click(function(){
		var href=$(this).data('href');	
		window.location.href=href;
	});
	$('.ifhref').click(function(){
		var iframeurl=$(this).data('href');	
		document.getElementById("myframe").src=iframeurl;
		
	}); 
	$(".yema li").click(function(){
		$(".yema li").removeClass('li-click');
		$(this).addClass('li-click');
	});
	//2-poster点击左边分类栏效果
	 $(".left-icon-list li").click(function(){
		//改变分类li颜色
		$(".left-icon-list").children('li').attr('class','left-icon-list-li');
		$(this).attr('class','left-icon-active');
		//改变图标颜色
		var id=$(this).data('id');
		var num=$(".left-icon-list li").length; 
		for(var i=1;i<=num;i++){
			document.getElementById('left-icon-img'+i).src="images/fail-"+i+".png"; 
		}
		document.getElementById('left-icon-img'+id).src="images/fail-"+id+"-1.png"; 
		
		
	});
	 //获取url中的参数
        function getUrlParam(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
            var r = window.location.search.substr(1).match(reg);  //匹配目标参数
            if (r != null) return unescape(r[2]); return null; //返回参数值
        }
	//根据链接改变页面点击项颜色
	var type=getUrlParam('type');
	if(type!=null){
		console.log(type);
		$('.head-nav li').eq(type-1).attr('class','nav-li-active jshref');
	}

	//分配筛选功能
    $('.top-nav li').click(function(){
        for(var k=1;k<9;k++){
            $("#select"+k).hide();
        }
    	var select=$(this).data('select');
    	var mun=select.length;
        for(var k=0;k<mun;k++){
            $("#select"+select[k]).css('display','inline-block');
        }
        $('.top-nav li').attr('class','li-noslect');
        $(this).attr('class','li-active');
    });

	
})