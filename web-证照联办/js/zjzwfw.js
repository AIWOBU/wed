/******************************
** 文件描述 :  浙江政务服务 交互
** 时    间 ： 2017.11
*******************************/
(function($) {
    var Zjzw = {
        init: function() {
            this.operates();
        },

        // 操作流程 交互
        operates: function() {
            var $body = $('.zjzw-body');
            $body.on('click', '.tip-close', function() {
                $body.find('.panel-operate-tip').addClass('hide');
            });

            $body.on('click', '.panel-operate-wrap .js-NextBtn', function() {
                var $this = $(this),
                    $parent = $this.closest('.panel-flow-body');

                $parent.addClass('hide').next('.panel-flow-body').removeClass('hide');
				
            });
        }
    };

    Zjzw.init();
}(jQuery));
