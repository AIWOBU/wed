// JavaScript Document
$(function(){
		$("#choice-place").click();
	//点击地区选择
	$(".span-hover").click(function(){
		$(this).siblings().attr('class','span-hover');
		$(this).attr('class','span-active');
		$(this).siblings().children('a').css('color','#333');
		$(this).children('a').css('color','white');
	});
	//点击图标选中项目
	$(".li-icon").click(function(){
		var id=$(this).data('id');
		if($('#checked'+id).is(':checked')) {   //如果该联办事项是选中的
			$("#icon-li"+id).attr('class','li-icon li-hover');  //更改li类，改变背景颜色为白
			document.getElementById("icon_img"+id).src="images/index-icon-"+id+".png";    //更改li中的图片链接
			document.getElementById('checked'+id).checked = false;
            document.getElementById('icon_lab'+id).checked = false;
			$("#label"+id).hide();  //底部该事项隐藏
		}else{
			 //如果该联办事项是未选中的
			$("#icon-li"+id).attr('class','li-icon li-active');   //更改li类，改变背景颜色为蓝色
			document.getElementById("icon_img"+id).src="images/index-icon-"+id+"-2.png";    //更改li中的图片链接
			$("#confirm").show();   //底部项目确认显示
			$(".icon-box1").show();   //底部项目确认显示
			$("#label"+id).show();   //底部该事项显示
			document.getElementById('checked'+id).checked = true;
            document.getElementById('icon_lab'+id).checked = true;
			$('#small-icon-list'+id).hide();
		}	
	});
	$('.position_lab').click(function(){
        var id=$(this).data('id');
        if($('#icon_lab'+id).is(':checked')) {   //如果该联办事项是选中的
            $("#icon-li"+id).attr('class','li-icon li-hover');  //更改li类，改变背景颜色为白
            document.getElementById("icon_img"+id).src="images/index-icon-"+id+".png";    //更改li中的图片链接
            document.getElementById('checked'+id).checked = false;
            document.getElementById('icon_lab'+id).checked = false;
            $("#label"+id).hide();  //底部该事项隐藏
        }else{
            //如果该联办事项是未选中的
            $("#icon-li"+id).attr('class','li-icon li-active');   //更改li类，改变背景颜色为蓝色
            document.getElementById("icon_img"+id).src="images/index-icon-"+id+"-2.png";    //更改li中的图片链接
            $("#confirm").show();   //底部项目确认显示
            $(".icon-box1").show();   //底部项目确认显示
            $("#label"+id).show();   //底部该事项显示
            document.getElementById('checked'+id).checked = true;
            document.getElementById('icon_lab'+id).checked = true;
            $('#small-icon-list'+id).hide();
        }
    })
	//遍历判断行高
    $(".icon-list li h4").each(function(){
    	console.log($(this).text());
        if($(this).text().length<=8){
        	$(this).css('line-height','30px');
		}else{
            $(this).css('font-size','14px');
            $(this).css('line-height','16px');
		}
    });
	//点击项目确认去除选中项目
	$("#confirm-list").on("click", ".checked-lab", function(){
		var id=$(this).data('id');
		$(this).hide();
			document.getElementById('checked'+id).checked = false;   //该事项为未选中状态
        	document.getElementById('icon_lab'+id).checked = false;
			$("#icon-li"+id).attr('class','li-hover');  //更改li类，改变背景颜色为白
			document.getElementById("icon_img"+id).src="images/index-icon-"+id+".png";    //更改li中的图片链接
	});
	$(".jshref").click(function(){
		var href=$(this).data('href');	
		window.location.href=href;
	});
	
	//点击左边分类栏效果
	$(".left-icon-list li").click(function(){
		//改变分类li颜色
		$(".left-icon-list li").attr('class','left-icon-list-li');
		$(this).attr('class','left-icon-active');
		//改变图标颜色
		var id=$(this).data('id');
		var num=$(".left-icon-list li").length; 
		for(var i=0;i<num;i++){
			document.getElementById('left-icon-img'+i).src="images/index-classify-"+i+"-2.png"; 
		}
		document.getElementById('left-icon-img'+id).src="images/index-classify-"+id+"-4.png"; 
		//改变top-tip文本内容
		var tip=$(this).data('tip');
		$('#top-tip').text(tip);
		//分类筛选
		if(id!=0){
			var classify=$(this).data('classify');
			$('.li-icon').hide();
			for(var j=0;j<classify.length;j++){
				$('#icon-li'+classify[j]).show();
			}	
		}else{
			$('.li-icon').show();
		}
		
	});

})