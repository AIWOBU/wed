// JavaScript Document
$(function(){
    //点击评价的星星
    $('.star_box span').click(function(){
        var num=$(this).data('num');
        //点亮星星
        for(var i=0; i<5;i++){
            if(i<num){
                $('.star_box span').eq(i).attr('class','star_orange');
            }else{
                $('.star_box span').eq(i).attr('class','star_gray');
            }
        }
        $('.button_box span').hide(); //评价小标签全部隐藏
        $('.start'+num).show();
        //改变评价等级
        if(num==1){
            $('.assess_result').html('非常不满意');

        }else if(num==2){
            $('.assess_result').html('不满意');
        }else if(num==3){
            $('.assess_result').html('基本满意');
        }else if(num==4){
            $('.assess_result').html('满意');
        }else if(num==5){
            $('.assess_result').html('非常满意');
        }

    });

    //点击小标签按钮
    $('.button_box span').click(function(){

        var a=$(this).hasClass('tab_active');
        console.log(a);
        if(a!=false){
            $(this).removeClass('tab_active').addClass('tab_read');
        }else{
            $(this).removeClass('tab_read').addClass('tab_active');
        }
    });



})